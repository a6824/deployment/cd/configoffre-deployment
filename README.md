# Template de déploiement de Chart Helm

Template basique de déploiement automatisé via helm

La chart et sa version peuvent être modifiées dans le fichier 'application'

Pour que le déploiement soit effectif, vous avez besoin d'un token de déploiement Rancher et de cibler le bon contexte lors du déploiement.

Pour cela, mettez l'identifiant de votre contexte dans la variable d'environnement RANCHER_CONTEXT, et votre token de déploiement dans la variable RANCHER_BEARER_TOKEN.  
Pour obtenir un contexte et un token de déploiement, contactez l'équipe yDevOps [contact@ydevops.fr](mailto:contact@ydevops.fr).

## Découpage des environements

Chaque environnement est lié à une branche. La branche 'dev' déploie dans l'environnement 'dev', la branche 'int' déploie dans l'environnement 'int' et ainsi de suite. 

Par conséquent, vous pouvez modifier les valeurs de chaque environnement dans leur fichiers spécifiques (dossiers 'int' et 'dev') et même rajouter des dossier et des jobs dans votre pipeline pour déployer dans plus d'environnements.  
Aussi, vous pouvez via les paramètres GitLab rajouter des variables d'environnements spécifiques à 'dev' ou 'int' pour modifier les valeurs d'une branche à l'autre.
